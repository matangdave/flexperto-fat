<?php

namespace frontend\models;
use yii\base\Model;
use Yii;
use common\models\User;

class UserProfileForm extends Model
{
	public $fname;
	public $lname;
	public $email;
	public $id;
	public $mobile;
	public $profilePic;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		// firstname , lastName
		[['fname', 'lname',], 'required'],				//first and last name is required
		[['fname', 'lname'], 'string', 'max' => 30],	// max 30 char
		[['mobile'], 'string', 'max' => 20],	// max 30 char
		[['mobile'], 'udokmeci\yii2PhoneValidator\PhoneValidator'], // mobile validator
		['email','email'],	// email has to be a valid email address

		];
	}

 	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email' => 'Email',
            'mobile' => 'Mobile'
        ];
    }

    /**
     * load UserProfileForm By getting data of UserModel
     *
     * @param int $id
     * @return \frontend\models\UserProfileForm
     */
    public static function loadModel($id) {

    	$user = User::findIdentity($id);

    	$model = new UserProfileForm();
    	$model->id 		= $user->id;
    	$model->fname 	= $user->fname;
    	$model->lname 	= $user->lname;
    	$model->mobile 	= $user->mobile;
    	$model->email 	= $user->email;

    	if($user->is_avatar_set) { //user has set avatar
    		$model->profilePic = "/img/avatar/$id.jpeg";
    	}
    	else {  // no avatar set -  show defaultavatar from params
    		$model->profilePic = \Yii::$app->params['defaultAvatar'];
    	}

    	return $model;
    }


}
?>