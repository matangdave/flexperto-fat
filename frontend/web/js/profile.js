

$(document).ready(function () {

	
	$(document.body).on('submit', '#crop_form', function (e) {
        var frm = $(this);
 
        //send request to profile image upload
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            dataType: 'json',
            data: frm.serialize(),
            success: function (data) {
                if (data && data.url) {
                   $("#js_save_profile_photo").css("visibility", "hidden");;
                   $(".imgareaselect-outer").hide();
                   $('.imgareaselect-border1').parent().hide()
                   $("#user_profile_photo").attr("src",data.url);
                   alert("Profile picture updated");
                }
            },
        });
        return false;
    });
 
 
})

$(window).bind("load", function() { 
	//remove image selector on load of page
    $(".imgareaselect-outer").hide();
    $('.imgareaselect-border1').parent().hide()
});	

// file uploaded
function onFileUploadSuccess (e, data) {
	$("#user_profile_photo").attr("src",data.result);
	$("#js_save_profile_photo").css("visibility", "visible");
	$("#profile_path").attr("value",data.result);
}

//file upload failed
function onFileUploadFail () {
	alert("File upload on server failed");
}
