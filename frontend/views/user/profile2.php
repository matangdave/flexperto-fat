<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$width = $height = 100;
$max_width = 250;
$alt_text = "No file";
$image_source = '/';

?>
<?= \cozumel\cropper\ImageCropper::widget(['id' => 'user_profile_photo']); ?>

<img width="<?= $width; ?>" height="<?= $height; ?>" max-width="<?=$max_width;?>" class="border" id="user_profile_photo" alt="<?=$alt_text;?>" src="<?= $image_source; ?>">
<?php


$form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['your/url/here']), 'options' => ['id' => 'crop_form'],
]);
?>
<div><input type="hidden" id="x1" value="" name="x1"></div>
<div><input type="hidden" id="y1" value="" name="y1"></div>
<div><input type="hidden" id="x2" value="" name="x2"></div>
<div><input type="hidden" id="y2" value="" name="y2"></div>
<div><input type="hidden" id="w" value="" name="w"></div>
<div><input type="hidden" id="h" value="" name="h"></div>
<div><input type="hidden" value="<?= $width; ?>" name="image_width"></div>
<div><input type="hidden" value="<?= $height; ?>" name="image_height"></div>
<div style="margin-top:10px;">
<input type="submit" class="button" id="js_save_profile_photo" value="Save Avatar">
</div>

<?php ActiveForm::end(); ?>



