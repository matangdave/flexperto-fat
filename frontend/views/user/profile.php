<?php

use dosamigos\fileupload\FileUpload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//include profile.js file -> depends on jquery
$this->registerJsFile('@web/js/profile.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$width = $height = 200;
$maxWidth = 400;
$altText = "Avatar";
$imageSource = $model->profilePic;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;


?>

<?=
	\cozumel\cropper\ImageCropper::widget(['id' => 'user_profile_photo']);

?>

<img width="<?= $width; ?>" height="<?= $height; ?>" max-width="<?=$maxWidth;?>" class="border" id="user_profile_photo" alt="<?=$altText;?>" src="<?= $imageSource; ?>">
<div style='height: 10px;width:100%'></div>
<?=
	FileUpload::widget([
    'model' => $model,
    'attribute' => 'profilePic',
    'url' => ['user/profile-pic'],
    'options' => ['accept' => 'image/*'],
    'clientOptions' => [
        'maxFileSize' => 2000000
    ],

    // Also, you can specify jQuery-File-Upload events
    // see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
    'clientEvents' => [
        'fileuploaddone' => 'onFileUploadSuccess',
        'fileuploadfail' => 'onFileUploadFail',
    ],
]);?>
<?php
$form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['user/set-avatar']),
		'options' => ['id' => 'crop_form'],
]);
?>
<div><input type="hidden" id="x1" value="" name="x1"></div>

<div><input type="hidden" id="y1" value="" name="y1"></div>
<div><input type="hidden" id="x2" value="" name="x2"></div>
<div><input type="hidden" id="y2" value="" name="y2"></div>
<div><input type="hidden" id="w" value="" name="w"></div>
<div><input type="hidden" id="h" value="" name="h"></div>
<div><input type="hidden" value="<?= $width; ?>" name="image_width"></div>
<div><input type="hidden" value="<?= $height; ?>" name="image_height"></div>
<div><input type="hidden" id="profile_path" value="" name="path"></div>
<div style="margin-top:10px;">

<div class="form-group">
     <?= Html::submitButton('Crop and Save', ['class' => 'btn btn-primary', 'name' => 'save-profile','style' => "visibility:hidden" , 'id'=>"js_save_profile_photo"]) ?>
</div>

<input type="submit" class="button" style="visibility:hidden" id="js_save_profile_photo" value="Crop and Sve">
</div>

<?php ActiveForm::end(); ?>

<div class="row">
        <div class="col-lg-5">
           <?php
			$form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['user/profile']), 'options' => ['id' => 'profile_form'],
]);
?>
                <?= $form->field($model, 'fname')->textInput(['maxlength'=>30]); ?>
                <?= $form->field($model, 'lname')->textInput(['maxlength'=>30]) ?>
                <?= $form->field($model, 'email')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength'=>20]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Update Profile', ['class' => 'btn btn-primary', 'name' => 'save-profile', 'value'=>'1']) ?>
                    <?= Html::submitButton('Delete Profile', ['class' => 'btn btn-primary', 'name' => 'delete-profile' , 'value'=>'1']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>






