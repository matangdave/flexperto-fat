<?php
namespace frontend\controllers;

use Yii;
use frontend\models\UserProfileForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\JsonParser;
use yii\helpers\Json;
use common\models\User;
use frontend\models\frontend\models;

/**
 * Site controller
 */
class UserController extends Controller
{

	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Component::behaviors()
	 */

	public function behaviors()
	{
		//allow access to only authorized users
		return [
				'access' => [
						'class' => \yii\filters\AccessControl::className(),
						'only' => ['profile', 'profilePic','setAvatar'],
						'rules' => [
								// allow authenticated users
								[
										'allow' => true,
										'roles' => ['@'],
								],
								// everything else is denied
						],
				],
		];
	}

	/**
	 * show user profile
	 *
	 */
	public function actionProfile()
	{
		$session = \Yii::$app->session;
		$session->open();
		$id = $session->get ( 'userId' );

		if (! empty ( $post = Yii::$app->request->post () )) {

			// request for delete profile
			if (! empty ( $post ['delete-profile'] )) {

				$model = User::findIdentity($id);
				$model->status = User::STATUS_DELETED;

				//update user status in db
				if($model->save()) {
					\Yii::$app->getSession()->setFlash('success', 'Your account has been delete.');
					return $this->redirect('/site/logout');
				}
				else { //error received
					\Yii::$app->getSession()->setFlash('error', 'Error while deleting your account!!');
				}

			} else { //request for update profile

				$post = Yii::$app->request->post ();
				$userProfile = $post ['UserProfileForm'];

				$model = User::findIdentity ( $id );
				$model->fname = $userProfile ['fname'];
				$model->lname = $userProfile ['lname'];
				$model->mobile = $userProfile ['mobile'];

				if ($model->save())
					\Yii::$app->getSession ()->setFlash ( 'success', 'Profile has been updated.' );
				else {
					\Yii::$app->getSession ()->setFlash ( 'error', 'Error while updating profile!!' );
				}
			}
		}

		$model = UserProfileForm::loadModel ( $id );
		return $this->render ( 'profile', [
				'model' => $model
		] );
	}

	/**
	 * function uploads file to a temporary folder to be croped afterward.
	 *
	 * @return string
	 */
	public function actionProfilePic() {

		$session =\Yii::$app->session;
		$session->open();
		$random = $session->getId() . rand(1,100);
		$imageFile = UploadedFile::getInstanceByName('UserProfileForm[profilePic]');
		$profileUrl = '/img/temp/';
		$directory = \Yii::getAlias('@'.\Yii::$app->params['avatarTempDir']);
		if (!is_dir($directory)) {
			mkdir($directory);
		}
		if ($imageFile)	 {

			$filePath = $directory .$random ;
			if ($imageFile->saveAs($filePath)) {
				chmod($filePath, 0777);
				return  $profileUrl.$random;
			}
		}
		return '';

	}

	/**
	 * function crops the file and uploads to its final destination.
	 */
	public function actionSetAvatar() {

		$session =\Yii::$app->session;
		$session->open();
		$sessionId = $session->getId();
		$newname = \Yii::getAlias('@'.\Yii::$app->params['avatarDir']).$session->get('userId').'.jpeg';

		//get the filename from the post data
		$request = Yii::$app->request;
		$filename = end(explode('/',$request->post('path')));

		$imageSource = \Yii::getAlias('@'.\Yii::$app->params['avatarTempDir']).$filename;
		$avatarUrl = '/img/avatar/'.$session->get('userId').'.jpeg?'.rand(1,100);

		//gather x and y cordinates of croped region
		$x1 = empty($request->post('x1'))?0:$request->post('x1');
		$x2 = empty($request->post('x2'))?0:$request->post('x2');
		$y1 = empty($request->post('y1'))?0:$request->post('y1');
		$y2 = empty($request->post('y2'))?0:$request->post('y2');

		$h = $request->post('h');
		$w = $request->post('w');

		// nothing cropped
		if (empty($w)) {
			if(file_exists($newname)) {
				@unlink($newname);
			}
			copy($imageSource, $newname);
			@unlink($imageSource);
		}
		else {

			$imageHeight = $request->post ( 'image_height' );
			$imageWidth = $request->post ( 'image_width' );

			//crop image
			$imagick = new \Imagick($imageSource);
			$imagick->cropImage($x2-$x1, $y2-$y1, $x1, $y1);

			//resize to orinal display
			$imagick->resizeImage($imageWidth, $imageHeight,\Imagick::FILTER_LANCZOS, 0.9);
			//delete file if already exists
			if(file_exists($newname)) {
				@unlink($newname);
			}
			//save file on the disk
			file_put_contents($newname, $imagick);

		}
		//Get user model
		$user = User::findIdentity($session->get('userId'));

		//update value of avatar if it is unset
		if($user->is_avatar_set == 0) {
			$user->is_avatar_set = 1;
			$user->save();
		}

		$response['url'] = $avatarUrl;
		return Json::encode($response,true);
	}

}