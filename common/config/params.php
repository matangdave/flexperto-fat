<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'defaultAvatar'=>'/img/default_avatar.jpg',
    'avatarTempDir'=>'frontend/web/img/temp/',
    'avatarDir'=>'frontend/web/img/avatar/'


];
