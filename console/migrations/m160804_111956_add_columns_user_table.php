<?php

use yii\db\Migration;

class m160804_111956_add_columns_user_table extends Migration
{
    public function up()
    {
    	$this->addColumn('user', 'mobile', $this->string(20));
    	$this->addColumn('user', 'is_avatar_set', $this->integer(1)->defaultValue(0));
    	$this->addColumn('user', 'fname', $this->string(30));
    	$this->addColumn('user', 'lname', $this->string(30));
    }

    public function down()
    {
        $this->dropColumn('user', 'mobile');
    	$this->dropColumn('user', 'is_avatar_set');
    	$this->dropColumn('user', 'fname');
    	$this->dropColumn('user', 'lname');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
